#!/bin/bash
set -o pipefail

#Get IP Address
. /etc/sysconfig/network*/ifcfg-e*
IPA=${IPADDR:0:-3}

#Operating System Version & Name
. /etc/os-release 2> /dev/null
echo "node_os_info{promURL=\"${IPA}\",id=\"${ID}\",id_like=\"${ID_LIKE}\",name=\"${NAME}\",pretty_name=\"${PRETTY_NAME}\",version=\"${VERSION}\",version_codename=\"${VERSION_CODENAME}\",version_id=\"${VERSION_ID}\"} 1"

#User Application Version Check
VER_UA=$(/usr/bin/test -f /opt/netiq/idm/apps/tomcat/webapps/idmdash/build-info.json && /usr/bin/cat /opt/netiq/idm/apps/tomcat/webapps/idmdash/build-info.json|awk -F\" '{print $20}')
if [ -z "$VER_UA" ] 
then 
    echo "user_app_version{version=\"N/A\"} 1"
else
    echo "user_app_version{version=\"$VER_UA\"} 1"
fi

#Identity Manager Version Check
IDM=$(rpm -qa|grep novell-DXMLengnnoarch)
VER_IDM=${IDM:22:7}
if [ -z "$VER_IDM" ] 
then 
    echo "idm_version{version=\"N/A\"} 1"
else
    echo "idm_version{version=\"$VER_IDM\"} 1"
fi

#eDirectory Version Check
EDIR=$(rpm -qa|grep patterns-edirectory)
EDIR2=${EDIR:20:20}
if [ -z "$EDIR2" ] 
then 
    echo "edir_version{version=\"N/A\"} 1"
else
    VER_EDIR=${EDIR2::-7}
    echo "edir_version{version=\"$VER_EDIR\"} 1"
fi

#Remote Loader Version Check
RL_PS=$(ps aux|grep rdxml | grep -v grep)
if [ -z "$RL_PS" ]
then 
    echo "remote_loader_version{version=\"N/A\"} 1"
else
    RL=$(rpm -qa|grep novell-DXMLrdxmlx-)
    RL2=${RL:18:20}
    VER_RL=${RL2::-7}
    echo "remote_loader_version{version=\"$VER_RL\"} 1"
fi