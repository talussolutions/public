@echo off
Title Download Windows node_exporter Binary
color 0A & Mode 60,3
set "workdir=C:\out"
If not exist %workdir% mkdir %workdir%
Set "URL=https://github.com/prometheus-community/windows_exporter/releases/download/v0.25.1/windows_exporter-0.25.1-amd64.msi"
Set "URL1=https://bitbucket.org/talussolutions/public/raw/fc706d695a07df525d0be1cdf255bce5f1f79f4e/password-filter-check.ps1"
Set "URL2=https://bitbucket.org/talussolutions/public/raw/6edc41013a838f39aa0534fc2d45cbbf20a81e96/schedule-setup.txt"
Set "FileLocation1=%workdir%\password-filter-check.ps1"
Set "FileLocation2=%workdir%\schedule-setup.txt"
Set "FileLocation=%workdir%\windows_exporter-0.25.1-amd64.msi"
echo(
echo    Please wait a while ... The download is in progress ...
Call :Download %URL% %FileLocation%
Call :Download %URL1% %FileLocation1%
Call :Download %URL2% %FileLocation2%
msiexec /i C:\out\windows_exporter-0.25.1-amd64.msi TEXTFILE_DIRS="C:\out"
start notepad.exe "C:\out\schedule-setup.txt" & Exit
echo Done
#Explorer /n,/select,"%FileLocation%" & Exit

::**************************************************************************
:Download <url> <File>
Powershell.exe ^
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'; ^
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols; ^
(New-Object System.Net.WebClient).DownloadFile('%1','%2')
exit /b
::**************************************************************************