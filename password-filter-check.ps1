Import-Module ActiveDirectory

$RegistryHive = 'LocalMachine'
$RegistryKeyPath = 'SOFTWARE\NOVELL\PwFilter'
$ValueName = 'Host Names'
$DomainName = (Get-ADDomain).DNSRoot
$SRVS = Get-ADDomainController -Filter {IsReadOnly -eq $false} -Server $DomainName|Select-Object Hostname

Clear-Content C:\out\password-filter.prom

ForEach($Srv in $SRVS) {
    $reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($RegistryHive, $Srv.Hostname)
    $key = $reg.OpenSubKey($RegistryKeyPath)

    if (($key -ne $null) -and ($key.GetValue($ValueName,$null) -ne $null)) 
    {
        $bodyTrue = 'password_filters{filter_installed = "1", filter_host="'+$Srv.Hostname+'"} 1'
        $bodyTrue | Add-Content C:\out\password-filter.prom -encoding utf8
    }
    Else { 
        $bodyFalse = 'password_filters{filter_installed = "1", filter_host="'+$Srv.Hostname+'"} 0'
        $bodyFalse | Add-Content C:\out\password-filter.prom -encoding utf8
    }
}