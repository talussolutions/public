#!/bin/bash
cd /root
useradd -m -s /bin/bash node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.7.0/node_exporter-1.7.0.linux-amd64.tar.gz
tar xvfz node_exporter-*.*-amd64.tar.gz
rm -f node_exporter-*.*-amd64.tar.gz
cp ./node_exporter-*.*-amd64/node_exporter /usr/sbin
rm -rf node_exporter-*.*-amd64/
touch /usr/lib/systemd/system/node-exporter.service
cat > /usr/lib/systemd/system/node-exporter.service << "EOF"
[Unit]
Description=Node Exporter

[Service]
User=node_exporter
EnvironmentFile=/etc/sysconfig/node_exporter
ExecStart=/usr/sbin/node_exporter $OPTIONS

[Install]
WantedBy=multi-user.target

EOF
touch /etc/sysconfig/node_exporter
cat > /etc/sysconfig/node_exporter << EOF
OPTIONS="--collector.textfile.directory /var/prometheus/node_exporter/"

EOF
mkdir -p /var/prometheus/node_exporter/
systemctl daemon-reload
systemctl enable node-exporter.service
systemctl start node-exporter
wget https://bitbucket.org/talussolutions/public/raw/66f27df1ce2de07ebce9224caea8de04edde64ba/versions.sh
chmod +x /root/versions.sh
echo "0 1 * * * root /root/versions.sh > /var/prometheus/node_exporter/versions.prom" >> /etc/crontab
echo "" >> /etc/crontab
/root/versions.sh > /var/prometheus/node_exporter/versions.prom
chown -Rf node_exporter:users /var/prometheus